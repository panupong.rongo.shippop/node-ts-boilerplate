import { greetings } from './greetings'

let name = ``

describe('มาเริ่ม test function greeting กันเธอ', () => {
  describe(`ถ้า name = tayalone`, () => {
    beforeEach(() => {
      // // arrange
      name = `tayalone`
    })
    test(`output ต้องเป็น 'Hi tayalone'`, () => {
      const assert = `Hi tayalone`
      // act
      const result: string = greetings(name)
      // aaert
      expect(result).toBe(assert)
    })
  })
})
